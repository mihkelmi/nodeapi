module.exports = function(app) {

	'use strict';

	var sequelize = require('../lib/sequelize'),
		Article,
		Author;

	Article = sequelize.import(__dirname + '/article');
	Author = sequelize.import(__dirname + '/author');

	Author.hasMany(Article);
	Article.belongsTo(Author, {foreignKey: 'authorId'});

	app.set('model.article', Article);
	app.set('model.author', Author);
};