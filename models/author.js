module.exports = function(sequelize, datatypes) {

	'use strict';

	return sequelize.define('Author', {

		id: {
			type: datatypes.INTEGER,
			primaryKey: true,
			autoIncrement: true
		},

		email: {
			type: datatypes.STRING,
			unique: true,
			validate: {
				isEmail: true
			}
		},

		name: {
			type: datatypes.STRING,
			validate: {
				notEmpty: true
			}
		}
	});
};