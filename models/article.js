module.exports = function(sequelize, datatypes) {

	'use strict';

	return sequelize.define('Article', {
		title: {
			type: datatypes.STRING,
			validate: {
				notEmpty: true
			},
		},

		lead: {
			type: datatypes.TEXT
		},

		body: {
			type: datatypes.TEXT,
			validate: {
				notNull: true
			}
		}
	});
};