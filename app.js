'use strict';

var Express = require('express'),
	bodyParser = require('body-parser'),
	config = require('./lib/config'),
	db = require('./lib/sequelize'),
	app = module.exports = new Express(),
	dbconn;

app.set('config', config);
app.set('db', db);

app.use(bodyParser());

app.all('*', function(req, res, next) {
	res.header('Access-Control-Allow-Origin', '*');
	res.header('Access-Control-Allow-Headers', 'X-Requested-With');
	res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE');
	next();
});

require('./models')(app);
require('./routes')(app);
require('./middleware/404')(app);

dbconn = db.authenticate();
dbconn.success(function() {
	console.info('Database connection established');

	if (require.main === module) {
		app.listen(3000, function() {
			console.info('Server running on port 3000');
		});
	}
});

dbconn.error(function(reason) {
	console.error('Database connection failed', reason);
});