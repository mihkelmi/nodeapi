/**
* API version 1
*/
module.exports = function(app) {

	'use strict';

	var router = require('express').Router();

	// Map resources
	router.use('/articles', require(__dirname + '/articles')(app));
	router.use('/authors', require(__dirname + '/authors')(app));

	return router;
};