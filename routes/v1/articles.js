module.exports = function(app) {

	'use strict';

	var router = require('express').Router(),
		Article = app.get('model.article'),
		Author = app.get('model.author'),
		_ = require('underscore');

	/**
	* Get all articles
	* @param object req
	* @param object res
	*/
	router.get('/', function allArticles(req, res) {

		var criteria = {};

		if (req.query.fields && req.query.fields.length) {
			criteria.attributes = req.query.fields.split(',');
		}

		if (req.query.withAuthor) {
			criteria.include = [Author];
		}

		criteria.limit = parseInt(req.query.limit, 10) || 25;
		criteria.offset = parseInt(req.query.offset, 10) || 0;

		var getAll = Article.findAll(criteria);

		getAll.success(function(articles) {
			return res.json(articles, 200);
		});

		getAll.error(function(reason) {
			return res.json(reason, 500);
		});
	});


	/**
	* Create new article
	* @param object req
	* @param object res
	*/
	router.post('/', function newArticle(req, res) {

		var article = Article.build({
			title: req.body.title,
			lead: req.body.lead,
			body: req.body.body
		});

		var author = Author.find(req.body.authorId);

		author.error(function(reason) {
			console.log('Unable to find author', reason);
			return res.json(reason, 500);
		});

		author.success(function(author) {

			var validation = article.validate();

			if (validation) {
				return res.json(validation, 400);
			}

			article.save()
				.success(function(article) {
					return res.json(article);
				})

				.error(function(reason) {
					return res.json(reason, 500);
				});

			article.setAuthor(author);
		});
	});


	/**
	* Get article by id
	* @param req
	* @param res
	*/
	router.get('/:id', function articleById(req, res) {

		var criteria = {where: {id: req.params.id}},
			article;

		if (req.query.withAuthor) {
			criteria.include = [Author];
		}

		if (req.query.fields && req.query.fields.length) {
			criteria.attributes = req.query.fields.split(',');
		}

		criteria.limit = 1;

		article = Article.findAll(criteria);

		article.success(function(article) {

			var status = 200;

			if (!article) {
				status = 404;
			}

			return res.json(article[0], status);
		});

		article.error(function(reason) {
			return res.json(reason);
		}, 500);
	});


	/**
	* Update article
	* @param object req
	* @param object res
	*/
	router.post('/:id', function updateArticle (req, res) {

		Article.find(req.params.id)

			.success(function(article) {

				if (!article) {
					res.json(null, 404);
				}

				_.each(req.body, function(value, key) {

					if (key !== 'id') {
						article[key] = value;
					}
				});

				var validation = article.validate();

				if (validation) {
					return res.json(validation, 400);
				}

				article.save()

					.success(function(article) {
						res.json(article);
					})

					.error(function(reason) {
						res.json(reason, 500);
					});
			})

			.error(function(reason) {
				res.json(reason, 500);
			});
	});

	return router;
};