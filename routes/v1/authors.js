module.exports = function(app) {

	'use strict';

	var router = require('express').Router(),
		Author = app.get('model.author'),
		Article = app.get('model.article'),
		_ = require('underscore');


	/**
	* Get all authors
	* @param object req
	* @param object res
	*/
	router.get('/', function allAuthors(req, res) {

		var fields = req.query.fields,
			criteria = {},
			authors;

		if (fields && fields.length) {
			criteria.attributes = fields.split(',');
		}

		criteria.limit = parseInt(req.query.limit, 10) || 25;
		criteria.offset = parseInt(req.query.offset, 10) || 0;

		authors = Author.findAll(criteria);

		authors.success(function(authors) {
			return res.json(authors);
		});

		authors.error(function(reason) {
			console.error('Failed to load authors', reason);
			return res.json(reason, 500);
		});
	});


	/**
	* Create new author
	* @param object req
	* @params object res
	*/
	router.post('/', function newAuthor(req, res) {

		var author = Author.build({
			email: req.body.email,
			name: req.body.name
		});

		var validation = author.validate();

		validation.error(function(reason) {
			return res.status(400).json(validation);
		}).success(function() {

			var save = author.save();

			save.success(function(author) {
				return res.json(author);
			});

			save.error(function(reason) {
				console.error('Failed to save new user', reason);
				return res.status(500).json(reason);
			});
		});
	});


	/**
	* Get author by id
	* @param object req
	* @param object res
	*/
	router.get('/:id', function authorById(req, res) {

		var author = Author.find(req.params.id);

		author.success(function(author) {

			if (!author) {
				return res.json(null, 404);
			}

			return res.json(author);
		});

		author.error(function(reason) {
			console.error('Failed to load author', reason);
			return res.json(reason, 500);
		});
	});


	/**
	* Update author
	* @param object req
	* @param object res
	*/
	router.post('/:id', function updateAuthor(req, res) {

		var author = Author.find(req.params.id);

		author.error(function(reason) {
			console.error('Failed to load author', reason);
			return res.json(reason, 500);
		});

		author.success(function(author) {

			_.each(req.params, function(value, key) {

				author[key] = value;

				var validation = author.validate();

				if (validation) {
					return res.json(validation, 400);
				}

				var save = author.save();

				save.success(function(author) {
					return res.json(author);
				});

				save.error(function(reason) {
					console.error('Failed to save user', reason);
					return res.json(reason, 500);
				});
			});
		});
	});


	router.get('/:id/articles', function authorsArticles(req, res) {

		var fields = req.query.fields,
			criteria = {where: {authorId: req.params.id}},
			articles;

		if (fields && fields.length) {
			criteria.attributes = fields.split(',');
		}

		criteria.limit = parseInt(req.query.limit, 10) || 25;
		criteria.offset = parseInt(req.query.offset, 10) || 0;

		articles = Article.findAll(criteria);

		articles.success(function(articles) {
			return res.json(articles);
		});

		articles.error(function(reason) {
			console.error('Failed to load articles for user', reason);
			return res.json(reason, 500);
		});
	});

	return router;
};