module.exports = function(app) {

	'use strict';

	var _ = require('underscore'),
		apiVersions = app.get('config').apiVersions || [];

	// load routes for all api versions
	_.each(apiVersions, function(version) {
		app.use('/' + version, require(__dirname + '/' + version)(app));
	});

	// use v1 as default
	app.use('/', require(__dirname + '/v1')(app));

	// global route for api versions
	app.get('/versions', function(req, res) {
		return res.json(apiVersions);
	});
};