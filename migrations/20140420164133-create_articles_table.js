'use strict';

var tableName = 'Articles';

module.exports = {
	up: function(migration, DataTypes, done) {
		migration.createTable(tableName, {
			id: {
				type: DataTypes.INTEGER,
				autoIncrement: true,
				primaryKey: true
			},

			name: {
				type: DataTypes.STRING,
				allowNull: false
			},

			lead: {
				type: DataTypes.TEXT,
				allowNull: true
			},

			body: {
				type: DataTypes.TEXT,
				allowNull: false
			},

			createdAt: {
				type: DataTypes.DATE
			},

			updatedAt: {
				type: DataTypes.DATE
			}
		});

		done();
	},

	down: function(migration, DataTypes, done) {
		migration.dropTable(tableName);
		done();
	}
};