'use strict';

module.exports = {
	up: function(migration, DataTypes, done) {

		migration.addColumn('Articles', 'authorId', {
			type: DataTypes.INTEGER
		});

		done();
	},

	down: function(migration, DataTypes, done) {

		migration.removeColumn('Articles', 'authorId');
		done();
	}
};