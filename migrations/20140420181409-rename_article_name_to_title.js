'use strict';

module.exports = {
	up: function(migration, DataTypes, done) {
		migration.renameColumn('Articles', 'name', 'title');
		done();
	},

	down: function(migration, DataTypes, done) {
		migration.renameColumn('Articles', 'title', 'name');
		done();
	}
};