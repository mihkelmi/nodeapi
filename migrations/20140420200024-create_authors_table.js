'use strict';

var tableName = 'Authors';

module.exports = {
	up: function(migration, DataTypes, done) {

		migration.createTable(tableName, {
			id: {
				type: DataTypes.INTEGER,
				autoIncrement: true,
				primaryKey: true
			},

			email: {
				type: DataTypes.STRING,
				unique: true,
				allowNull: false
			},

			name: {
				type: DataTypes.STRING,
				allowNull: false
			},

			createdAt: {
				type: DataTypes.DATE
			},

			updatedAt: {
				type: DataTypes.DATE
			}
		});

		done();
	},

	down: function(migration, DataTypes, done) {
		migration.dropTable(tableName);
		done();
	}
};