module.exports = function(app) {

	'use strict';

	// 404 handler
	app.use(function(req, res) {
		res.json('route does not exist', 404);
	});
};