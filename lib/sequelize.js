var Sequelize = require('sequelize'),
	config = require('./config'),
	env = process.env.NODE_ENV || config.env || 'development',
	sequelize;

if (typeof config[env] === 'undefined') {
	throw new Error('Config has no environment section for ' + env);
}

config = config[env];

sequelize = new Sequelize(config.database, config.username, config.password, {
	dialect: config.dialect
});

module.exports = sequelize;