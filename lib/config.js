var fs = require('fs'),
	config = null;

if (config === null) {
	config = fs.readFileSync('./config/config.json', { encoding: 'utf8'});
	config = JSON.parse(config);
}

module.exports = config;