'use strict';

var expect = require('expect.js'),
	request = require('supertest'),
	app = require('../../app.js');


describe('Articles', function() {

	describe('GET /v1/articles', function() {

		it('should respond with json', function(done) {

			request(app)
				.get('/v1/articles')
				.set('Accept', 'application/json')
				.expect('Content-Type', /json/)
				.expect(200)
				.expect(function(res) {
					expect(res.body).to.be.an('array');
					expect(res.body[0]).to.be.an('object');
					expect(res.body[0]).to.have.property('lead');
					expect(res.body[0]).to.have.property('title');
				})
				.end(done);
		});
	});
});